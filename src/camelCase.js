/**
 * This function turns the specified string into camel cased identifier. Camel case (stylized
 * as camelCase; also known as camel caps or more formally as medial capitals) is the practice
 * of writing phrases such that each word or abbreviation in the middle of the phrase begins
 * with a capital letter, with no intervening spaces or punctuation. For example.
 *
 * - `safe hTML` -> `safeHtml`
 * - `escape HTML entities` -> `escapeHtmlEntities`
 *
 * The identifier should only contains english letters (`A` to `Z`, including upper and
 * lower case), digits (`0` to `9`) and underscore (`_`). Other characters will be treated
 * as delimiters. For example.
 *
 * - `safe+html` -> `safeHtml`
 *
 * @param {String} string The input string.
 */
export default function camelCase (string) {
  // TODO:
  //   Please implement the function.
  // <-start-
  if (string === null) {
    return null;
  } else if (string === undefined || string.length === 0) {
    return undefined;
  } else {
    return string.replace(/[^\w\d]/g, ' ').split(' ').map(function (word, index) {
      if (index === 0) {
        return word.toLowerCase();
      }
      return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    }).join('');
  }
  // --end-->
}

// TODO:
//   You can add additional code here if you want
// <-start-

// --end-->
